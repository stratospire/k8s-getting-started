FROM node:8.1.4-alpine

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV
COPY src/package.json /usr/src/app/
RUN npm install && npm cache clean --force
COPY src/. /usr/src/app

CMD [ "npm", "start" ]
